import ResizeObserver from 'resize-observer-polyfill';

/**
 * Service class to send height information of the plug-in to COYO.
 */
export class HeightAdapter {
  private static readonly RES_SUBJECT = 'setHeight';
  protected observer: ResizeObserver | null = null;

  /**
   * Creates a new instance of this class.
   *
   * @param srcId the plug-in source ID to use for requests
   * @returns the new instance
   */
  constructor(private readonly srcId: string) {}

  /**
   * Connects this adapter to the HTML element that matches the given selectors,
   * observes its height and sends throttled update messages to COYO.
   *
   * @param selectors the selectors to target the HTML element
   * @param throttle a small timeframe to throttle messages
   */
  connect(selectors: string, throttle: number): void {
    this.disconnect();
    this.observer = new ResizeObserver(
      this.throttle((entries: ResizeObserverEntry[]) => {
        const height = entries[0].contentRect.height;
        parent.postMessage({ iss: this.srcId, sub: HeightAdapter.RES_SUBJECT, height }, '*');
      }, throttle)
    );
    const elem = document.querySelector(selectors);
    if (elem) {
      this.observer.observe(elem);
    }
  }

  /**
   * Disconnects this adapter from all HTML elements.
   */
  disconnect(): void {
    if (this.observer) {
      this.observer.disconnect();
      this.observer = null;
    }
  }

  private throttle(cb: ResizeObserverCallback, wait: number): ResizeObserverCallback {
    let timeout: number | null = null;
    return (entries: ResizeObserverEntry[], observer: ResizeObserver): void => {
      if (timeout === null) {
        cb(entries, observer);
        timeout = window.setTimeout(() => (timeout = null), wait);
      }
    };
  }
}
