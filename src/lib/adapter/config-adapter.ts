import { showError, showErrorAndRecover } from '../error/error-handlers';
import { JwkStore } from '../jwt/jwk-store';
import { JwtHandler } from '../jwt/jwt-handler';
import { JsonObject } from '../utils/json';
import { randomStr } from '../utils/random';

/**
 * Service class to react to changes in the config form on COYO side.
 */
export class ConfigAdapter {
  private static readonly REQ_SUBJECT = 'save';
  private static readonly RES_SUBJECT = 'setConfig';
  protected listener: ((event: MessageEvent) => Promise<void>) | null = null;
  protected jwkStore: JwkStore;
  protected jwtHandler: JwtHandler;
  protected userId: string | null = null;

  /**
   * Creates a new instance of this class.
   *
   * @param srcId the plug-in source ID to use for requests
   * @returns the new instance
   */
  constructor(private readonly srcId: string) {
    this.jwkStore = JwkStore.getInstance();
    this.jwtHandler = new JwtHandler();
  }

  /**
   * Bind this instance to a specific user ID to only accept tokens that define
   * the same user ID via "ctx.userId".
   *
   * @param userId the user ID
   */
  bind(userId: string): void {
    this.userId = userId;
  }

  /**
   * Connects the given callback listeners to config form events coming from COYO.
   *
   * @param cb a callback to be executed when the form should be saved
   * @param validate a flag indicating if the JWT response should be validated
   */
  connect(cb: (respond: (data: boolean | JsonObject) => void, token: string) => void, validate: boolean): void {
    this.disconnect();
    this.listener = (event: MessageEvent) => {
      const claims = this.jwtHandler.decodeJwtClaims(event.data);
      if (claims && claims['sub'] === ConfigAdapter.REQ_SUBJECT && claims['ctx.userId'] === this.userId) {
        const jti = claims.jti || randomStr();
        const respond = (data: boolean | JsonObject) =>
          parent.postMessage({ iss: this.srcId, sub: ConfigAdapter.RES_SUBJECT, jti, data }, '*');
        if (validate) {
          return this.jwtHandler
            .validateJwt(claims, {})
            .then(() => {
              const header = this.jwtHandler.decodeJwtHeader(event.data);
              return this.jwkStore
                .getJwk(claims.iss || '', header ? header.jku : undefined)
                .then(keys => this.jwtHandler.verifyJwt(event.data, ...keys))
                .catch(err => (err.recover ? showErrorAndRecover(err, void 0) : Promise.reject(err)));
            })
            .then(() => cb(respond, event.data))
            .catch(err => showError(err));
        } else {
          cb(respond, event.data);
        }
      }
      return Promise.resolve();
    };
    window.addEventListener('message', this.listener);
  }

  /**
   * Disconnects all registered listeners.
   */
  disconnect(): void {
    if (this.listener) {
      window.removeEventListener('message', this.listener);
      this.listener = null;
    }
  }
}
