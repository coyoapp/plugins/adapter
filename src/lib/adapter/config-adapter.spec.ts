import { createSpyObj } from 'jest-createspyobj';
import { mocked } from 'jest-mock';

import { JwkStore } from '../jwt/jwk-store';
import { JwtHandler } from '../jwt/jwt-handler';

import { ConfigAdapter } from './config-adapter';

jest.mock('../jwt/jwt-handler');
jest.mock('../jwt/jwk-store');
jest.mock('../utils/random', () => ({ randomStr: () => 'jti' }));

describe('ConfigAdapter', () => {
  let configAdapter: ConfigAdapter;
  let jwtHandlerMock: jest.Mocked<JwtHandler>;
  let jwkStoreMock: jest.Mocked<JwkStore>;

  beforeEach(() => {
    mocked(JwtHandler).mockImplementation(() => jwtHandlerMock);
    jwtHandlerMock = createSpyObj(JwtHandler);
    mocked(JwkStore).getInstance.mockImplementation(() => jwkStoreMock);
    jwkStoreMock = createSpyObj(JwkStore);
    configAdapter = new ConfigAdapter('srcId');
  });

  test('should be defined', () => {
    expect(configAdapter).toBeDefined();
  });
});
