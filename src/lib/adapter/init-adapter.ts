import { showError, showErrorAndRecover } from '../error/error-handlers';
import { PluginError, PluginErrorCode } from '../error/plugin-error';
import { JwkStore } from '../jwt/jwk-store';
import { JwtClaims } from '../jwt/jwt-claims';
import { JwtHandler } from '../jwt/jwt-handler';
import { randomStr } from '../utils/random';

/**
 * Service class to initialize a plug-in and retrieve initialization response
 * information.
 */
export class InitAdapter {
  private static readonly REQ_SUBJECT = 'init';
  private static readonly RES_SUBJECT = 'init';
  protected jwkStore: JwkStore;
  protected jwtHandler: JwtHandler;

  /**
   * Creates a new instance of this class.
   *
   * @param srcId the plug-in source ID to use for requests
   * @returns the new instance
   */
  constructor(private readonly srcId: string) {
    this.jwkStore = JwkStore.getInstance();
    this.jwtHandler = new JwtHandler();
  }

  /**
   * Sends an initialization message to the parent window (i.e. COYO)
   * and retrieves the response.
   *
   * @param timeout a maximum time to wait for the response
   * @param validate a flag indicating if the JWT response should be validated
   * @returns a promise holding the decoded (and possibly validated) JWT claims
   */
  send(timeout: number, validate: boolean): Promise<[JwtClaims, string]> {
    return new Promise((resolve, reject) => {
      let listener: ((event: MessageEvent) => void) | null = null;
      let timer: number | null = null;
      const clear = () => {
        if (listener !== null) {
          window.removeEventListener('message', listener);
        }
        if (timer !== null) {
          clearTimeout(timer);
        }
      };

      const jti = randomStr();
      listener = (event: MessageEvent) => {
        const claims = this.jwtHandler.decodeJwtClaims(event.data);
        if (claims && claims.jti === jti) {
          clear();
          if (validate) {
            this.jwtHandler
              .validateJwt(claims, { sub: InitAdapter.REQ_SUBJECT })
              .then(() => {
                const header = this.jwtHandler.decodeJwtHeader(event.data);
                return this.jwkStore
                  .getJwk(claims.iss || '', header ? header.jku : undefined)
                  .then(keys => this.jwtHandler.verifyJwt(event.data, ...keys))
                  .catch(err => (err.recover ? showErrorAndRecover(err, void 0) : Promise.reject(err)));
              })
              .then(() => resolve([claims, event.data]))
              .catch(err => showError(err));
          } else {
            resolve([claims, event.data]);
          }
        }
      };

      timer = window.setTimeout(() => {
        clear();
        reject(new PluginError(PluginErrorCode.MessageTimeout, `Initialization timed out after ${timeout}ms`));
      }, timeout);

      window.addEventListener('message', listener);
      parent.postMessage({ iss: this.srcId, sub: InitAdapter.RES_SUBJECT, jti }, '*');
    });
  }
}
