import { createSpyObj } from 'jest-createspyobj';
import { mocked } from 'jest-mock';

import { JwkStore } from '../jwt/jwk-store';
import { JwtHandler } from '../jwt/jwt-handler';

import { InitAdapter } from './init-adapter';

jest.mock('../jwt/jwt-handler');
jest.mock('../jwt/jwk-store');
jest.mock('../utils/random', () => ({ randomStr: () => 'jti' }));

describe('InitAdapter', () => {
  let initAdapter: InitAdapter;
  let jwtHandlerMock: jest.Mocked<JwtHandler>;
  let jwkStoreMock: jest.Mocked<JwkStore>;

  beforeEach(() => {
    mocked(JwtHandler).mockImplementation(() => jwtHandlerMock);
    jwtHandlerMock = createSpyObj(JwtHandler);
    mocked(JwkStore).getInstance.mockImplementation(() => jwkStoreMock);
    jwkStoreMock = createSpyObj(JwkStore);
    initAdapter = new InitAdapter('srcId');
  });

  test('should be defined', () => {
    expect(initAdapter).toBeDefined();
  });

  test('should send', () => {
    const timeout = 1000;
    const validate = true;

    jwtHandlerMock.decodeJwtClaims.mockReturnValue({ jti: 'jti' });
    jwtHandlerMock.decodeJwtHeader.mockReturnValue({});
    jwtHandlerMock.validateJwt.mockResolvedValue();
    jwtHandlerMock.verifyJwt.mockResolvedValue();
    jwkStoreMock.getJwk.mockResolvedValue([{} as CryptoKey]);

    const result = initAdapter.send(timeout, validate);

    window.postMessage('token', '*');

    return result;
  });
});
