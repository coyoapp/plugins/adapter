import { createSpyObj } from 'jest-createspyobj';
import { mocked } from 'jest-mock';
import ResizeObserver from 'resize-observer-polyfill';

import { HeightAdapter } from './height-adapter';

jest.mock('resize-observer-polyfill');
class HeightAdapterTest extends HeightAdapter {
  set _observer(observer: ResizeObserver | null) {
    this.observer = observer;
  }

  get _observer(): ResizeObserver | null {
    return this.observer;
  }
}

describe('HeightAdapter', () => {
  let heightAdapterTest: HeightAdapterTest;
  let resizeObserverMock: jest.Mocked<ResizeObserver>;

  beforeEach(() => {
    resizeObserverMock = createSpyObj(ResizeObserver);
    mocked(ResizeObserver).mockImplementation(() => resizeObserverMock);
    heightAdapterTest = new HeightAdapterTest('srcId');
  });

  test('should connect', () => {
    const elem = {} as Element;
    const querySelectorSpy = jest.spyOn(document, 'querySelector').mockReturnValueOnce(elem);
    const selectors = 'selectors';
    const throttle = 500;

    heightAdapterTest.connect(selectors, throttle);

    expect(querySelectorSpy).toHaveBeenCalledWith(selectors);
    expect(resizeObserverMock.observe).toHaveBeenCalledWith(elem);
  });

  test('should disconnect', () => {
    const observer = createSpyObj(ResizeObserver);
    heightAdapterTest._observer = observer;

    heightAdapterTest.disconnect();

    expect(observer.disconnect).toHaveBeenCalled();
    expect(heightAdapterTest._observer).toBeNull();
  });
});
