import { randomStr } from './random';

describe('random', () => {
  test('should generate a random string', () => {
    expect(randomStr().length).toEqual(16);
    expect(randomStr(8).length).toEqual(8);
  });
});
