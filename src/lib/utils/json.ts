export type JsonPrimitive = boolean | number | string | null | undefined;

export type JsonValue = JsonPrimitive | JsonObject | Array<JsonValue>;

export interface JsonObject {
  [key: string]: JsonValue;
}
