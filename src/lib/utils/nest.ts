import { JwtClaims } from '../jwt/jwt-claims';

/**
 * Converts the given claims into a nested JS object for esier access.
 *
 * @param claims the JWT token claims
 * @returns the nested JWT token claims
 */
export function nest(claims: JwtClaims): JwtClaims {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const rest: { [key: string]: any } = {};
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const nest: { [key: string]: any } = {};

  for (const key in claims) {
    if (key.match(/^(ctx|cfg|stt)\./)) {
      const _key = key.substr(0, 3);
      if (!nest[_key]) {
        nest[_key] = {};
      }
      nest[_key][key.substr(4)] = claims[key];
    } else {
      rest[key] = claims[key];
    }
  }

  return { ...rest, ...nest };
}
