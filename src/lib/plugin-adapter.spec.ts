import { createSpyObj } from 'jest-createspyobj';
import { mocked } from 'jest-mock';

import { ConfigAdapter } from './adapter/config-adapter';
import { HeightAdapter } from './adapter/height-adapter';
import { InitAdapter } from './adapter/init-adapter';
import { JWK_PROD_RGX, JWK_PROD_V3, JWK_PROD_V4, JWK_STAGE_RGX, JWK_STAGE_V3, JWK_STAGE_V4 } from './jwt/jwk-defaults';
import { JwkStore } from './jwt/jwk-store';
import { PluginAdapter } from './plugin-adapter';

jest.mock('./jwt/jwk-store');
jest.mock('./adapter/init-adapter');
jest.mock('./adapter/config-adapter');
jest.mock('./adapter/height-adapter');

describe('PluginAdapter', () => {
  let windowSpy: jest.SpyInstance<Window>;
  let jwkStoreMock: jest.Mocked<JwkStore>;
  let initAdapterMock: jest.Mocked<InitAdapter>;
  let configAdapterMock: jest.Mocked<ConfigAdapter>;
  let heightAdapterMock: jest.Mocked<HeightAdapter>;
  let pluginAdapter: PluginAdapter;

  beforeEach(() => {
    windowSpy = jest.spyOn(window, 'window', 'get');

    mocked(JwkStore).getInstance.mockImplementation(() => jwkStoreMock);
    jwkStoreMock = createSpyObj(JwkStore);
    mocked(InitAdapter).mockImplementation(() => initAdapterMock);
    initAdapterMock = createSpyObj(InitAdapter);
    mocked(ConfigAdapter).mockImplementation(() => configAdapterMock);
    configAdapterMock = createSpyObj(ConfigAdapter);
    mocked(HeightAdapter).mockImplementation(() => heightAdapterMock);
    heightAdapterMock = createSpyObj(HeightAdapter);

    pluginAdapter = new PluginAdapter('srcId');
  });

  afterEach(() => {
    windowSpy.mockRestore();
  });

  test('should use the given src ID', () => {
    expect(pluginAdapter.srcId).toBe('srcId');
  });

  test('should use the query src ID', () => {
    windowSpy.mockImplementation(
      () =>
        ({
          location: {
            href: 'https://example.com?src=querySrcId'
          }
        } as any)
    );

    pluginAdapter = new PluginAdapter();

    expect(pluginAdapter.srcId).toBe('querySrcId');
  });

  test('should fallback without any src ID', () => {
    pluginAdapter = new PluginAdapter();

    expect(pluginAdapter.srcId).toBe('');
  });

  test('should register hosts', () => {
    expect(jwkStoreMock.addHost.mock.calls).toContainEqual(['https://certificates.plugins.coyoapp.com']);
    expect(jwkStoreMock.addHost.mock.calls).toContainEqual(['https://plugins.coyoapp.com']);
  });

  test('should register keys', () => {
    expect(jwkStoreMock.addJwk.mock.calls).toContainEqual([JWK_STAGE_RGX, JWK_STAGE_V4, JWK_STAGE_V3]);
    expect(jwkStoreMock.addJwk.mock.calls).toContainEqual([JWK_PROD_RGX, JWK_PROD_V4, JWK_PROD_V3]);
  });

  test('should add JWK', () => {
    const reg = /^http/i;
    const jwk = {
      key: {},
      alg: {
        name: 'algName',
        hash: 'algHash'
      }
    };

    pluginAdapter.addJwk(reg, jwk);

    expect(jwkStoreMock.addJwk).toHaveBeenCalledWith(reg, jwk);
  });

  test('should add JWK host', () => {
    const url = 'http://example.com';

    pluginAdapter.addJwkHost(url);

    expect(jwkStoreMock.addHost).toHaveBeenCalledWith(url);
  });

  test('should init', () => {
    const claims = {
      sub: 'subject',
      'ctx.userId': 'userID',
      'cfg.text': 'Hello World',
      'cfg.bool': true,
      'stt.edit': false
    };
    const token = 'token';
    initAdapterMock.send.mockReturnValueOnce(Promise.resolve([claims, token]));

    const result = pluginAdapter.init();

    return result.then(v => {
      expect(initAdapterMock.send).toBeCalledWith(60000, true);
      expect(v).toStrictEqual({
        claims: {
          cfg: {
            bool: true,
            text: 'Hello World'
          },
          ctx: {
            userId: 'userID'
          },
          stt: {
            edit: false
          },
          sub: 'subject'
        },
        token: 'token',
        validated: true
      });
    });
  });

  test('should connect to config adapter', () => {
    const cb = () => {
      /* empty */
    };
    const result = pluginAdapter.onSave(cb);

    expect(configAdapterMock.connect).toHaveBeenCalledWith(cb, true);

    result();

    expect(configAdapterMock.disconnect).toHaveBeenCalled();
  });

  test('should connect to height adapter', () => {
    const result = pluginAdapter.observeHeight();

    expect(heightAdapterMock.connect).toHaveBeenCalledWith('html', 50);

    result();

    expect(heightAdapterMock.disconnect).toHaveBeenCalled();
  });
});
