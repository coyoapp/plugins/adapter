import { mock } from 'jest-mock-extended';

import { PluginError, PluginErrorCode } from '../error/plugin-error';

import { Jwk } from './jwk';
import { JwkStore } from './jwk-store';

class JwkStoreTest extends JwkStore {
  static new(): JwkStoreTest {
    return new JwkStoreTest();
  }

  get _localCryptoKeys(): [RegExp, Jwk[]][] {
    return this.localCryptoKeys;
  }

  get _remoteCryptoOrigins(): string[] {
    return this.remoteCryptoOrigins;
  }

  get _remoteCryptoCache(): Map<string, CryptoKey> {
    return this.remoteCryptoCache;
  }
}

describe('JwkStore', () => {
  let windowSpy: jest.SpyInstance<Window>;
  let subtle: jest.Mocked<SubtleCrypto>;
  let jwkStoreTest: JwkStoreTest;
  let xhr: jest.Mocked<XMLHttpRequest>;

  beforeEach(() => {
    windowSpy = jest.spyOn(global, 'window', 'get');
    subtle = mock<SubtleCrypto>();
    jwkStoreTest = JwkStoreTest.new();
    windowSpy.mockImplementation(() => ({ crypto: { subtle } } as any));
  });

  test('should get instance', () => {
    const result1 = JwkStore.getInstance();
    const result2 = JwkStore.getInstance();

    expect(result1).toBeInstanceOf(JwkStore);
    expect(result2).toBeInstanceOf(JwkStore);
    expect(result1).toBe(result2);
  });

  test('should add host', () => {
    const url = 'http://localhost/path/to/key.json';

    jwkStoreTest.addHost(url);

    expect(jwkStoreTest._remoteCryptoOrigins).toContain('http://localhost');
  });

  test('should add JWK', () => {
    const test = /^https?:\/\/localhost/i;
    const alg = { name: 'alg', hash: 'hash' };
    const key = { alg: 'alg' };

    jwkStoreTest.addJwk(test, { alg, key });

    expect(jwkStoreTest._localCryptoKeys).toContainEqual([test, [{ alg, key }]]);
  });

  test('should get JWK', () => {
    const alg = { name: 'alg', hash: 'hash' };
    const key = { alg: 'alg' };
    const cryptoKey = {} as CryptoKey;

    subtle.importKey.mockReturnValueOnce(Promise.resolve(cryptoKey));
    jwkStoreTest._localCryptoKeys.push([/^https?:\/\/localhost/i, [{ alg, key }]]);

    const result = jwkStoreTest.getJwk('http://localhost');

    return result.then(v => {
      expect(v).toStrictEqual([cryptoKey]);
    });
  });

  test('should get JWK from remote', () => {
    const alg = { name: 'alg', hash: 'hash' };
    const key = { alg: 'alg' };
    const jku = 'http://test.com/path/to/key.json';
    const cryptoKey = {} as CryptoKey;

    subtle.importKey.mockReturnValueOnce(Promise.resolve(cryptoKey));
    jwkStoreTest._remoteCryptoOrigins.push('http://test.com');
    mockXhr(200, JSON.stringify({ alg, key }));
    const result = jwkStoreTest.getJwk('http://test.com', jku);

    return result.then(v => {
      expect(xhr.open).toHaveBeenCalledWith('GET', jku, true);
      expect(xhr.timeout).toBe(60000);
      expect(subtle.importKey).toHaveBeenCalledWith('jwk', key, alg, false, ['verify']);
      expect(jwkStoreTest._remoteCryptoCache.get(jku)).toBe(cryptoKey);
      expect(v).toStrictEqual([cryptoKey]);
    });
  });

  test('should get JWK from cache', () => {
    const jku = 'http://test.com/path/to/key.json';
    const cryptoKey = {} as CryptoKey;

    jwkStoreTest._remoteCryptoOrigins.push('http://test.com');
    jwkStoreTest._remoteCryptoCache.set(jku, cryptoKey);
    const result = jwkStoreTest.getJwk('http://test.com', jku);

    return result.then(v => {
      expect(v).toStrictEqual([cryptoKey]);
    });
  });

  test('should not get JWK from untrusted origin', () => {
    const jku = 'http://test.com/path/to/key.json';

    const result = jwkStoreTest.getJwk('http://test.com', jku);

    expect.assertions(2);
    return result.catch(e => {
      expect(e).toBeInstanceOf(PluginError);
      expect(e.code).toBe(PluginErrorCode.CryptoKeyNotTrusted);
    });
  });

  test('should handle XHR request errors', () => {
    const jku = 'http://test.com/path/to/key.json';
    const cryptoKey = {} as CryptoKey;

    subtle.importKey.mockReturnValueOnce(Promise.resolve(cryptoKey));
    jwkStoreTest._remoteCryptoOrigins.push('http://test.com');
    mockXhr(400);
    const result = jwkStoreTest.getJwk('http://test.com', jku);

    expect.assertions(2);
    return result.catch(e => {
      expect(e).toBeInstanceOf(PluginError);
      expect(e.code).toBe(PluginErrorCode.CryptoKeyNotFoundRemote);
    });
  });

  test('should not get unknown JWK', () => {
    const result = jwkStoreTest.getJwk('http://test.com');

    expect.assertions(2);
    return result.catch(e => {
      expect(e).toBeInstanceOf(PluginError);
      expect(e.code).toBe(PluginErrorCode.CryptoKeyNotFound);
    });
  });

  test('should not get JWK if SubtleCrypto is unavailable', () => {
    const alg = { name: 'alg', hash: 'hash' };
    const key = { alg: 'alg' };

    windowSpy.mockRestore();
    jwkStoreTest._localCryptoKeys.push([/^https?:\/\/localhost/i, [{ alg, key }]]);

    const result = jwkStoreTest.getJwk('http://localhost');

    expect.assertions(2);
    return result.catch(e => {
      expect(e).toBeInstanceOf(PluginError);
      expect(e.code).toBe(PluginErrorCode.CryptoNotFound);
    });
  });

  test('should reset', () => {
    const alg = { name: 'alg', hash: 'hash' };
    const key = { alg: 'alg' };

    jwkStoreTest._localCryptoKeys.push([/^https?:\/\/localhost/i, [{ alg, key }]]);

    jwkStoreTest.reset();

    expect(jwkStoreTest._localCryptoKeys.length).toBe(0);
  });

  function mockXhr(status: number, responseText?: string) {
    xhr = mock<XMLHttpRequest>({
      open: jest.fn(),
      send: jest.fn(),
      status,
      responseText
    });

    global.XMLHttpRequest = jest.fn(() => xhr) as any;

    setTimeout(() => {
      if (status >= 200 && status < 300) {
        (xhr as any)['onload']();
      } else {
        (xhr as any)['onerror']();
      }
    }, 0);
  }
});
