import { PluginError, PluginErrorCode } from '../error/plugin-error';

import { Jwk } from './jwk';

/**
 * Singleton service class to register and retrieve JSON Web Keys.
 */
export class JwkStore {
  private static instance: JwkStore;
  protected localCryptoKeys: [RegExp, Jwk[]][] = [];
  protected remoteCryptoOrigins: string[] = [];
  protected remoteCryptoCache: Map<string, CryptoKey> = new Map();

  protected constructor() {
    // hide from the outside world
  }

  /**
   * Retrieves the singleton instance of this service class.
   *
   * @returns the singleton instance
   */
  static getInstance(): JwkStore {
    if (!JwkStore.instance) {
      JwkStore.instance = new JwkStore();
    }
    return JwkStore.instance;
  }

  /**
   * Register a new trustworthy URL to retrieve JSON Web Keys from.
   *
   * @param url the URL to register
   */
  addHost(url: string): void {
    this.remoteCryptoOrigins.push(new URL(url).origin);
  }

  /**
   * Registers a new set of JSON Web Keys in the store. It is possible to
   * register multiple keys for a single environment in order to account for
   * key renewals with a transition period.
   *
   * @param test a regular expression to check if this key is applicable
   * @param jwk the JSON Web Key(s) to register
   */
  addJwk(test: RegExp, ...jwk: Jwk[]): void {
    this.localCryptoKeys.push([test, jwk]);
  }

  /**
   * Retrieves a set of JSON Web Keys for the given environment URL.
   *
   * @param iss the environment URL provided in the `iss` claim of the token
   * @returns a promise holding the corresponding Crypto keys
   */
  getJwk(iss: string, jku?: string): Promise<CryptoKey[]> {
    if (!window.crypto || !window.crypto.subtle) {
      return Promise.reject(new PluginError(PluginErrorCode.CryptoNotFound, 'Insecure plug-in communication', true));
    }

    const keys = this.localCryptoKeys.find(([regEx]) => iss.match(regEx));
    if (keys) {
      const cryptos = keys[1].map(key => window.crypto.subtle.importKey('jwk', key.key, key.alg, false, ['verify']));
      return Promise.all(cryptos);
    } else if (jku) {
      return this.remoteCryptoOrigins.indexOf(new URL(jku).origin) !== -1
        ? this.loadJwk(jku).then(key => [key])
        : Promise.reject(new PluginError(PluginErrorCode.CryptoKeyNotTrusted, 'Insecure plug-in communication', true));
    } else {
      return Promise.reject(new PluginError(PluginErrorCode.CryptoKeyNotFound, 'Insecure plug-in communication', true));
    }
  }

  /**
   * Retrieves a JSON Web Key from the given URL.
   *
   * @param url the remote URL that hosts the JWK data
   * @param timeout the request timeout in milliseconds
   * @returns a promise holding the corresponding Crypto key
   */
  loadJwk(url: string, timeout = 60000): Promise<CryptoKey> {
    const cached = this.remoteCryptoCache.get(url);
    if (cached) {
      return Promise.resolve(cached);
    }

    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const store = this;
    return new Promise(function (resolve, reject) {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);
      xhr.timeout = timeout;
      xhr.onload = function () {
        const jwk = JSON.parse(xhr.responseText) as Jwk;
        window.crypto.subtle.importKey('jwk', jwk.key, jwk.alg, false, ['verify']).then(crypto => {
          store.remoteCryptoCache.set(url, crypto);
          resolve(crypto);
        });
      };
      xhr.onerror = function () {
        reject(new PluginError(PluginErrorCode.CryptoKeyNotFoundRemote, 'Insecure plug-in communication', true));
      };
      xhr.send();
    });
  }

  /**
   * Resets the store.
   */
  reset(): void {
    this.localCryptoKeys = [];
    this.remoteCryptoOrigins = [];
    this.remoteCryptoCache.clear();
  }
}
