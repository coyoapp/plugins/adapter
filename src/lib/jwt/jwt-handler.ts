import jwt_decode from 'jwt-decode';
import { base64url } from 'rfc4648';

import { PluginError, PluginErrorCode } from '../error/plugin-error';

import { JwtClaims } from './jwt-claims';
import { JwtHeader } from './jwt-header';

/**
 * Service class providing functionality to handle and verify JSON Web Tokens.
 */
export class JwtHandler {
  /**
   * Decodes a Base64Url encoded JSON Web Token header.
   *
   * @param token the JWT token
   * @returns the decoded JSON header parameters or `null` if it is not a JWT token
   */
  decodeJwtHeader(token: string): JwtHeader | null {
    return this.isJwt(token) ? jwt_decode<JwtHeader>(token, { header: true }) : null;
  }

  /**
   * Decodes a Base64Url encoded JSON Web Token.
   *
   * @param token the JWT token
   * @returns the decoded JSON object or `null` if it is not a JWT token
   */
  decodeJwtClaims(token: string): JwtClaims | null {
    return this.isJwt(token) ? jwt_decode<JwtClaims>(token) : null;
  }

  /**
   * Verifies the signature of the given JSON Web Token against a set of keys.
   * Only one of the keys needs to match in order to resolve the resulting
   * promise.
   *
   * @param token the JWT token
   * @param key the set of keys to check the token against
   * @returns an empty promise that resolves if the token is valid
   */
  verifyJwt(token: string, ...keys: CryptoKey[]): Promise<void> {
    const encoder = new window.TextEncoder();
    const [header, payload, signature] = token.split('.');
    const data = encoder.encode(`${header}.${payload}`);
    const sign = base64url.parse(signature, { loose: true });

    if (!window.crypto || !window.crypto.subtle) {
      Promise.reject(new PluginError(PluginErrorCode.CryptoNotFound, 'Insecure plug-in communication', true));
    }

    return Promise.any(
      keys.map(key =>
        window.crypto.subtle
          .verify(key.algorithm, key, sign, data)
          .then(valid => (valid ? Promise.resolve() : Promise.reject()))
      )
    ).catch(() => Promise.reject(new PluginError(PluginErrorCode.InvalidTokenSignature, 'Invalid plug-in token')));
  }

  /**
   * Validates the given decoded JWT token. Checks if the given claims align
   * with the validation claims. Furthermore the token's `nbf` and `exp` claims
   * are checked if they are defined in the token. A small leeway, usually no
   * more than a few minutes, may be used to account for clock skew.
   *
   * @param claims the decoded JWT token
   * @param validation the reference claims to check
   * @param leeway a small leeway to account for clock skew
   * @returns an empty promise that resolves if the token is valid
   */
  validateJwt(
    claims: JwtClaims,
    validation: { iss?: string; sub?: string; aud?: string; jti?: string },
    leeway = 60000
  ): Promise<void> {
    const validationClaims: Array<keyof typeof validation> = ['iss', 'sub', 'aud', 'jti'];
    for (const claim of validationClaims) {
      if (validation[claim] && validation[claim] !== claims[claim]) {
        return Promise.reject(new PluginError(PluginErrorCode.InvalidTokenClaim, 'Invalid plug-in token'));
      }
    }

    const now = Date.now();
    if (claims.exp && claims.exp >= now + leeway) {
      return Promise.reject(new PluginError(PluginErrorCode.InvalidTokenExp, 'Invalid plug-in token'));
    } else if (claims.nbf && claims.nbf < now - leeway) {
      return Promise.reject(new PluginError(PluginErrorCode.InvalidTokenNbf, 'Invalid plug-in token'));
    }

    return Promise.resolve();
  }

  private isJwt(token: string): boolean {
    return typeof token === 'string' && token.match(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/) !== null;
  }
}
