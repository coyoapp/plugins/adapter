// -----
// - JSON Web Keys (JWKs) generated via https://russelldavies.github.io/jwk-creator/
// -----

import { Jwk } from './jwk';

/** @ignore */
export const JWK_STAGE_RGX = /^https?:\/\/(?!next(?:-pmm)?\.coyostaging\.com)[a-z0-9_-]+\.coyostaging\.com/i;
/** @ignore */
export const JWK_STAGE_V3: Jwk = {
  alg: {
    name: 'RSASSA-PKCS1-v1_5',
    hash: { name: 'SHA-512' }
  },
  key: {
    kty: 'RSA',
    n:
      'p8kxlRYtYJDiRHzkswshoUvTJrHdFqvCe9dkszMc6fmWZ62Wk-VQ5l_BXlKWwFhcCYTkr9-mZt5Qgpnl' +
      'VsTrImlCkIAF7bEfNGywYPjf9PY-L-8riGNHD7VCneFpJYWOff4g1uANz0IsY9wA6TtzzGTp_WUn3zFj' +
      'pAFQdGsuMfOTz7wJt6lJRoJiXCeoz_33Po4zqjCIzwQ9BWBCad-8dMJ7OGkrN1O5gY-OBRCZjwS7PV_w' +
      'iU9veRix5Sb4M4k9hN8JJt6eCWMi2Uq1-AxA_XtOXH9AaRpYAbcmZGUcL6zDn1rf8KbTKTkUgsqiP-qo' +
      'fTphZGMVJRzTppGckFucMgqlOkXE1nvu-EPNKwKqTwmHuLVILMHWTzdGSCGaKR67pezb1pcRPuLTGhfB' +
      'W-aqo-cV2_FSxZ7pJLNrAiGMXty0vPG43TiwkTdvbWe5GY0wQelM1pbMGOHIJDiVNKZVKXSl697KLVX2' +
      '-iB1-47-Syp4STjc8bTT0a5c0sVsw5JVxqob30d11hJHDM3HrOJ6BJcuGhXgwMosHjvN3Lifc-pevspz' +
      'Yp0okfaFceR1bBIQrJSDNEQsjPsv9mvxwHPs4pUngu9jEn63y3KSAAB51itZJPbgU4jZFpn7LrSvNuCY' +
      'XFhHia2paQy2w53GObRcgayqhgk4O2Yh_p5l8ogENL8',
    e: 'AQAB',
    alg: 'RS512',
    use: 'sig'
  }
};
/** @ignore */
export const JWK_STAGE_V4: Jwk = {
  alg: {
    name: 'RSASSA-PKCS1-v1_5',
    hash: { name: 'SHA-512' }
  },
  key: {
    kty: 'RSA',
    n:
      'o1GMPdAZwJrlYzlaFS3jS3uLZbHbvf-9_bDRIipfvseS0OSeWnUVVvjkX3g2Jp8dZ71Ke0_ewZXSCvcV' +
      'r-9XVpWEm9O25j-6ysDZNtorSs13GIxm3_63ZPGEydbOcvKu-6MlhuHcpDikypGw_hWVEUcaydRYzWwq' +
      'GaBjJJUmbnRxGDZJCE87D-uKhTiTea8CohndCKl3suTuAVNuSvcx-Ad59g67Y0gF4Hbf2aSOjtR2dhZ4' +
      'VZ0b4KhDR4cDajnYmZVp-w85lIqCiC70JijmcJN1PnBUvTBZycrdpddJZoyfeqr5f4lHb5b-jBg6Yer8' +
      'fg-xRTrtc5jC4_PP0WdccLMAFoJSeArRMuOm4r5FB72cmP849Y3532XTq4JcJWBqbhTClNuGuRTLlh0n' +
      'M0DMjc8FuQDmgwnSO1u6W_n8sNa1x53UghgCwE7zqzKKD2GGYGMXsmtsyiCn5H9-hqRwoyYpcn8blFv-' +
      'iLzs-NEwQTiseKuR5QutXPIZr0LIs_kM4zaFBtcOKZvyV_o2eBlDBqcwzxmIr-iTYkT7Oz3StVf1jUtZ' +
      'wrJpNgNmmdaEDC2AoxN7HRjCGvewRUIpvI8Hktz6DhlWoosY93BnjBnu8qMfAmCjlfpCHwl68DlJGlQc' +
      'Suo0-Pjz2ShFfrO1CegnSVTWzuG5TtBtluDnp3HsRRE',
    e: 'AQAB',
    alg: 'RS512',
    use: 'sig'
  }
};

/** @ignore */
export const JWK_PROD_RGX = /^https?:\/\/[a-z0-9_-]+\.coyocloud.com/i;
/** @ignore */
export const JWK_PROD_V3: Jwk = {
  alg: {
    name: 'RSASSA-PKCS1-v1_5',
    hash: { name: 'SHA-512' }
  },
  key: {
    kty: 'RSA',
    n:
      '06zUMwTqZ1oBNtnYWR_2xRhQqv0ThGN4iAjVAe9SFt9jq0hHoug3qPt9MSWSzlIht1iACWRGlUAFR2x4' +
      'bYkJS4R_gTViglQUOhQf2cS2hV3hoa71wbAVQSSP5_Tnx8yVBhMuPlOwRNoMQDhfDqshp4qKQtW5ELF7' +
      'gMxsTAauq729Pyp97nyyYmmXuwuEyW2UACxpuwJ-56m9i8T8TIVsg1q5lO2HD6UBD2h2q0HbuCTan8KS' +
      'MYurm29U-38cVHaD06XMbSJlBz87pgCJd59zzVlN24PzKNSKgHW8KIq2-cWdRFosKToTSuGMvTm9GgUn' +
      'uM1HcZZ4bzDsvDI2ppnz5FtujAS3OPPHgnPH6xNR-C_Ew9vvAcmS9uVJ8KGveynUXux0Uc80lZAY-lcY' +
      'l_mhVR3XLGBA9IKTD3lOFWn7orVGqFEdP45xbwUM4uhwY7buEOn35PCVVuuBv-SPhO4oQ5Vk9LUXe_P9' +
      'Bqnx4ksbl1reHNx-O3AjTnkCTMF5vvJA3gJkwKzlJLG13YTRMaTJWSRkuu4nLzfgKKmaroTaf8IWoKI3' +
      'jTwH0zK4qu16f8XEF52xOf4GHqKt2GxwJvgg_W7q8C2P1NsiCa1ENj419k1damfVVUZ8WN7eVRoSFopA' +
      'WrUX0kbu4jWaK75gPPzJ6z4GOnkEC2hGocpsXUQqk10',
    e: 'AQAB',
    alg: 'RS512',
    use: 'sig'
  }
};
/** @ignore */
export const JWK_PROD_V4: Jwk = {
  alg: {
    name: 'RSASSA-PKCS1-v1_5',
    hash: { name: 'SHA-512' }
  },
  key: {
    kty: 'RSA',
    n:
      '8pQOErW0Sqcp-g0jyMk7KwYen6CuDhaxUWdOKQDTgUnl4JxXqmzCdXi2vNyZePXACeu-7Gca9qM6HlHY' +
      'OyfJoTKJvDinWOBXc4H2ijnpTFANmi-luSbGiUPvNWBQEjSZfqQcCjJ7MPVOyxtmzOjFRSpAOScHGTNa' +
      'GZiCQChI6UC6CkBIqdujzXR-pI0pQlDXgOgJ2TEhW9MORecovfqO-V1K8bIYlduL91EF5z3gwTd_Zsh_' +
      'f81x7CsomNJKEQ3S3ActBraTn0QFvxwhO0Daa4OUmoe_Mwc-HLkIbaZA3Q4U1RZneHMt54F9G3nGCmqS' +
      'Sgx4S0QfTxjpjHrp1PLke-5S_mbsWebd_OBRnlHkYtNNry6_wwx1puTl12CnWwtLLO7tUyMrfSB6GlWn' +
      'STjAww_9CzJq3qbqiUYNJHhXM07-d2YVwOAmreMEw6OCyRbTr4OY1vHNmuMCGdxAAyHWKa6RJczsZHbQ' +
      '40MSqvrP_KrLWMUaCHu_1iEIIGx30GEJcps0ZKxvrhRMLtcTXbN8xQGiBfjIboh6Nw36ngIUYqSLOShF' +
      'XqZaagnJKu6hfH94KnMPeFBHe3yww7FO8z-Af_cvGk2-7v1pp3Ar4O53dBN1wJLKFYaiSYBoj55ll6e9' +
      'MIhwYejCUmFmJ6wpFybgMJAufNrI1D2rSvmpphzYikk',
    e: 'AQAB',
    alg: 'RS512',
    use: 'sig'
  }
};
