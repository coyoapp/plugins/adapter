export interface Jwk {
  /**
   * The algorithm used for this JSON Web Key.
   */
  alg: RsaHashedImportParams;

  /**
   * The JSON Web Key.
   */
  key: JsonWebKey;
}
