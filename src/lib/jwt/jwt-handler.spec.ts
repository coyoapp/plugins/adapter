import { mocked } from 'jest-mock';
import { mock } from 'jest-mock-extended';
import jwt_decode from 'jwt-decode';

import { JwtHandler } from './jwt-handler';

jest.mock('jwt-decode');
jest.mock('rfc4648');

describe('JwtHandler', () => {
  let windowSpy: jest.SpyInstance<Window>;
  let subtle: jest.Mocked<SubtleCrypto>;
  let jwtHandler: JwtHandler;

  beforeEach(() => {
    windowSpy = jest.spyOn(global, 'window', 'get');
    subtle = mock<SubtleCrypto>();
    jwtHandler = new JwtHandler();
    windowSpy.mockImplementation(() => ({ crypto: { subtle } } as any));
  });

  afterEach(() => {
    mocked(jwt_decode).mockRestore();
  });

  test('should decode JWT token header', () => {
    const token = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjMifQ.VzdiNEOc4GJMhU5ovtnioXMXWD6SJzdkaGtY32NaEuk';
    const parameters = { alg: 'HS256' };
    mocked(jwt_decode).mockReturnValue(parameters);

    const result = jwtHandler.decodeJwtHeader(token);

    expect(jwt_decode).toHaveBeenCalledWith(token, { header: true });
    expect(result).toEqual(parameters);
  });

  test('should decode JWT token claims', () => {
    const token = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjMifQ.VzdiNEOc4GJMhU5ovtnioXMXWD6SJzdkaGtY32NaEuk';
    const claims = { iss: 'issuer' };
    mocked(jwt_decode).mockReturnValue(claims);

    const result = jwtHandler.decodeJwtClaims(token);

    expect(jwt_decode).toHaveBeenCalledWith(token);
    expect(result).toEqual(claims);
  });

  test('should return null for invalid tokens when decoding header', () => {
    const token1 = 'Hello World';
    const token2 = 123;

    const result1 = jwtHandler.decodeJwtHeader(token1);
    const result2 = jwtHandler.decodeJwtHeader(token2 as any);

    expect(jwt_decode).not.toHaveBeenCalled();
    expect(result1).toBeNull();
    expect(result2).toBeNull();
  });

  test('should return null for invalid tokens when decoding claims', () => {
    const token1 = 'Hello World';
    const token2 = 123;

    const result1 = jwtHandler.decodeJwtClaims(token1);
    const result2 = jwtHandler.decodeJwtClaims(token2 as any);

    expect(jwt_decode).not.toHaveBeenCalled();
    expect(result1).toBeNull();
    expect(result2).toBeNull();
  });
});
