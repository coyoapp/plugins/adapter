export interface JwtHeader {
  /**
   * The "typ" parameter defines the type of the JWT.
   */
  readonly typ?: string;

  /**
   * The "alg" parameter identifies the algorithm used to sign the JWT.
   */
  readonly alg?: string;

  /**
   * The "jku" parameter defines a URL to retrieve the JSON Web Key used to sign the JWT.
   */
  readonly jku?: string;
}
