export interface JwtClaims {
  /**
   * The "iss" (issuer) claim identifies the principal that issued the JWT.
   */
  readonly iss?: string;

  /**
   * The "sub" (subject) claim identifies the principal that is the subject of
   * the JWT.
   */
  readonly sub?: string;

  /**
   * The "aud" (audience) claim identifies the recipients that the JWT is
   * intended for.
   */
  readonly aud?: string;

  /**
   * The "exp" (expiration time) claim identifies the expiration time on or
   * after which the JWT MUST NOT be accepted for processing.
   */
  readonly exp?: number;

  /**
   * The "nbf" (not before) claim identifies the time before which the JWT MUST
   * NOT be accepted for processing.
   */
  readonly nbf?: number;

  /**
   * The "iat" (issued at) claim identifies the time at which the JWT was
   * issued.
   */
  readonly iat?: number;

  /**
   * The "jti" (JWT ID) claim provides a unique identifier for the JWT.
   */
  readonly jti?: string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  readonly [claims: string]: any;
}
