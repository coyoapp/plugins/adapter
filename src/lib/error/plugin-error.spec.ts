import { PluginError } from './plugin-error';

describe('PluginError', () => {
  test('should create', () => {
    const result = new PluginError(0, 'Test error');

    expect(result.name).toEqual('PluginError');
    expect(result.code).toEqual(0);
    expect(result.message).toEqual('Test error (ERR0)');
  });
});
