/** @ignore */
const OVERLAY_ID = 'cypl-ovl';
/** @ignore */
const ERROR_ID = 'cypl-err';

/**
 * Promise handler to show an error and reject the promise.
 *
 * @param reason the promise rejection error
 * @returns a rejected promise
 */
export function showError<T>(reason: T): Promise<T> {
  addOverlay();
  addError(getMessage(reason));
  return Promise.reject(reason);
}

/**
 * Promise handler to show an error and resolve the promise with a recover
 * value.
 *
 * @param reason the promise rejection error
 * @param recoverWith the recover value
 * @returns a resolved promise
 */
export function showErrorAndRecover<T, R>(reason: T, recoverWith: R): Promise<R> {
  addError(getMessage(reason));
  return Promise.resolve(recoverWith);
}

/**
 * Builds an error message from the given error.
 *
 * @param reason the error reason to be converted into a readable message
 * @returns a readable message
 */
function getMessage<T>(reason: T): string {
  return reason instanceof Error ? reason.message : `Internal error (${reason})`;
}

/**
 * Adds an overlay over the plug-in.
 */
function addOverlay(): void {
  destroy(OVERLAY_ID);
  const pos = 'position:absolute;top:0;left:0;right:0;bottom:0;z-index:9998;';
  const bg = 'background-color:#fff;opacity:.75;';
  const elem = create(`<div id="${OVERLAY_ID}" style="${pos}${bg}"></div>`);
  if (elem) {
    document.body.appendChild(elem);
  }
}

/**
 * Displays an error message above the plug-in.
 *
 * @param message the message to display
 */
function addError(message: string): void {
  destroy(ERROR_ID);
  const pos = 'position:absolute;top:0;left:0;z-index:9999;display:flex;align-items:center;';
  const font = "font:600 15px/24px 'Source Sans Pro', sans-serif;color:#cc4b2b;";
  const icon =
    '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="height:24px;width:24px;margin-right:4px;">' +
    '<path fill="#cc4b2b" d="M14.881 3.001a2 2 0 011.415.584l4.104 4.09a2 2 0 01.588 1.413l.01 5.793a2 2 0 01-.583 1.415l-4.09 4.104a2 2 0 01-1.413.588l-5.793.01a2 2 0 01-1.415-.583L3.6 16.325a2 2 0 01-.588-1.413l-.01-5.793a2 2 0 01.583-1.415L7.675 3.6a2 2 0 011.413-.588l5.793-.01zM12 15a1.25 1.25 0 100 2.5 1.25 1.25 0 000-2.5zm0-8.042c-.575 0-1.042.467-1.042 1.042v4.167a1.042 1.042 0 002.084 0V8c0-.575-.467-1.042-1.042-1.042z"/>' +
    '</svg>';
  const elem = create(`<div id="${ERROR_ID}" style="${pos}${font}">${icon}${message}</div>`);
  if (elem) {
    document.body.appendChild(elem);
  }
}

/**
 * Creates a new HTML fragment from an HTML string.
 *
 * @param html the HTML code to be parsed
 * @returns an HTML node or null if the code is invalid
 */
function create(html: string): Node | null {
  const temp = document.createElement('template');
  html = html.trim();
  temp.innerHTML = html;
  return temp.content.firstChild;
}

/**
 * Removes the HTML element with the given ID from the DOM.
 *
 * @param id the ID of the element to be removed
 */
function destroy(id: string): void {
  const elem = document.getElementById(id);
  if (elem) {
    elem.remove();
  }
}
