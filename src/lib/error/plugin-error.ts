/**
 * A list of error codes for plug-in related errors.
 */
export enum PluginErrorCode {
  /**
   * The SubtleCrypto interface of the Web Crypto API is not available.
   */
  CryptoNotFound,
  /**
   * No CryptoKey could be found for the corresponding environment URL in the JWK store.
   */
  CryptoKeyNotFound,
  /**
   * The token signature is invalid.
   */
  InvalidTokenSignature,
  /**
   * A token claim is invalid.
   */
  InvalidTokenClaim,
  /**
   * The token is expired.
   */
  InvalidTokenExp,
  /**
   * The token is premature.
   */
  InvalidTokenNbf,
  /**
   * The message request timed out.
   */
  MessageTimeout,
  /**
   * No CryptoKey could be found at the given URL.
   */
  CryptoKeyNotFoundRemote,
  /**
   * URL for CryptoKey not trusted.
   */
  CryptoKeyNotTrusted
}

/**
 * A plug-in related error thrown in the context of this adapter.
 */
export class PluginError extends Error {
  /**
   * Creates a new instance of this class.
   *
   * @param code the error code
   * @param message the error message
   * @param recover whether the app can recover from the error
   * @returns the new instance
   */
  constructor(readonly code: PluginErrorCode, message: string, readonly recover: boolean = false) {
    super(`${message} (ERR${code})`);
    this.name = 'PluginError';
  }
}
