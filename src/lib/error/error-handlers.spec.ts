import { showError, showErrorAndRecover } from './error-handlers';
import { PluginError } from './plugin-error';

describe('showError', () => {
  let documentSpy: jest.SpyInstance<Document>;

  beforeEach(() => {
    documentSpy = jest.spyOn(global, 'document', 'get');
  });

  afterEach(() => {
    documentSpy.mockRestore();
  });

  test('should reject with the same error', () => {
    const error = new PluginError(0, 'Test error');

    const result = showError(error);

    expect.assertions(1);
    return result.catch(e => {
      expect(e).toBe(error);
    });
  });

  test('should add an overlay and a message', () => {
    const remove = jest.fn();
    const getElementById = jest.fn();
    const createElement = jest.fn();
    const appendChild = jest.fn();
    const firstChild = {};

    getElementById.mockReturnValue({ remove });
    createElement.mockReturnValue({ content: { firstChild } });
    documentSpy.mockImplementation(
      () =>
        ({
          getElementById,
          createElement,
          body: { appendChild }
        } as any)
    );

    showError(new Error('Test error')).catch(() => {
      // ignore error
    });

    expect(getElementById.mock.calls[0][0]).toBe('cypl-ovl');
    expect(getElementById.mock.calls[1][0]).toBe('cypl-err');
    expect(remove.mock.calls.length).toBe(2);
    expect(createElement.mock.calls.length).toBe(2);
    expect(appendChild.mock.calls[0][0]).toBe(firstChild);
    expect(appendChild.mock.calls[1][0]).toBe(firstChild);
  });
});

describe('showErrorAndRecover', () => {
  let documentSpy: jest.SpyInstance<Document>;

  beforeEach(() => {
    documentSpy = jest.spyOn(global, 'document', 'get');
  });

  afterEach(() => {
    documentSpy.mockRestore();
  });

  test('should resolve with the recover value', () => {
    const error = new PluginError(0, 'Test error');
    const value = 42;

    const result = showErrorAndRecover(error, value);

    return result.then(v => {
      expect(v).toBe(value);
    });
  });

  test('should add a message', () => {
    const remove = jest.fn();
    const getElementById = jest.fn();
    const createElement = jest.fn();
    const appendChild = jest.fn();
    const firstChild = {};

    getElementById.mockReturnValue({ remove });
    createElement.mockReturnValue({ content: { firstChild } });

    documentSpy.mockImplementation(
      () =>
        ({
          getElementById,
          createElement,
          body: { appendChild }
        } as any)
    );

    showErrorAndRecover('Test error', 42);

    expect(getElementById.mock.calls[0][0]).toBe('cypl-err');
    expect(remove.mock.calls.length).toBe(1);
    expect(createElement.mock.calls.length).toBe(1);
    expect(appendChild.mock.calls[0][0]).toBe(firstChild);
  });
});
