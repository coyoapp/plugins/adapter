import { ConfigAdapter } from './adapter/config-adapter';
import { HeightAdapter } from './adapter/height-adapter';
import { InitAdapter } from './adapter/init-adapter';
import { Jwk } from './jwt/jwk';
import { JWK_PROD_RGX, JWK_PROD_V3, JWK_PROD_V4, JWK_STAGE_RGX, JWK_STAGE_V3, JWK_STAGE_V4 } from './jwt/jwk-defaults';
import { JwkStore } from './jwt/jwk-store';
import { JwtClaims } from './jwt/jwt-claims';
import { JsonObject } from './utils/json';
import { nest } from './utils/nest';

/**
 * The main service class to be used by a plug-in to establish a safe and secure
 * communication to the COYO front end bridge. It acts as a unified interaction
 * interface to all other plug-in handlers.
 */
export class PluginAdapter {
  private static readonly CONFIG_SRC_SUFFIX: string = '-cfg';
  private jwkStore: JwkStore;
  private initAdapter: InitAdapter;
  private initConfigAdapter: InitAdapter;
  private heightAdapter: HeightAdapter;
  private heightConfigAdapter: HeightAdapter;
  private configAdapter: ConfigAdapter;

  readonly srcId: string;

  /**
   * Creates a new instance of this class.
   *
   * @param srcId the plug-in source ID to use for requests
   * @returns the new instance
   */
  constructor(srcId?: string) {
    this.srcId = srcId || new URL(window.location.href).searchParams.get('src') || '';
    this.jwkStore = JwkStore.getInstance();
    this.initAdapter = new InitAdapter(this.srcId);
    this.initConfigAdapter = new InitAdapter(this.srcId + PluginAdapter.CONFIG_SRC_SUFFIX);
    this.heightAdapter = new HeightAdapter(this.srcId);
    this.heightConfigAdapter = new HeightAdapter(this.srcId + PluginAdapter.CONFIG_SRC_SUFFIX);
    this.configAdapter = new ConfigAdapter(this.srcId + PluginAdapter.CONFIG_SRC_SUFFIX);

    this.jwkStore.addHost('https://plugins.coyoapp.com');
    this.jwkStore.addHost('https://certificates.plugins.coyoapp.com');

    this.jwkStore.addJwk(JWK_PROD_RGX, JWK_PROD_V4, JWK_PROD_V3);
    this.jwkStore.addJwk(JWK_STAGE_RGX, JWK_STAGE_V4, JWK_STAGE_V3);
  }

  /**
   * Registers a new set of JSON Web Keys in the store. It is possible to
   * register multiple keys for a single environment in order to account for
   * key renewals with a transition period.
   *
   * @param test a regular expression to check if this key is applicable
   * @param jwk the JSON Web Key(s) to register
   */
  addJwk(test: RegExp, ...jwk: Jwk[]): void {
    this.jwkStore.addJwk(test, ...jwk);
  }

  /**
   * Register a new trustworthy URL to retrieve JSON Web Keys from.
   *
   * @param url the URL to register
   */
  addJwkHost(url: string): void {
    this.jwkStore.addHost(url);
  }

  /**
   * Sends an initialization message to the parent window (i.e. COYO)
   * and retrieves the decoded (and possibly validated) response as well as the
   * raw token response.
   *
   * @param config a flag indicating if the plugin configuration view should be initialized
   * @param timeout a maximum time to wait for the response
   * @param validate a flag indicating if the JWT response should be validated
   * @returns a promise holding the response data
   */
  init(
    config = false,
    timeout = 60000,
    validate = true
  ): Promise<{
    claims: JwtClaims;
    token: string;
    validated: boolean;
  }> {
    const adapter = config ? this.initConfigAdapter : this.initAdapter;
    return adapter.send(timeout, validate).then(([claims, token]) => {
      if (config) {
        this.configAdapter.bind(claims['ctx.userId']);
      }
      return { claims: nest(claims), token, validated: validate };
    });
  }

  /**
   * Starts to listen to COYO requests to save the current configuration.
   * Provides a `respond` callback that is used to respond to the received
   * message. The response can be `true` to indicate success without providing
   * any data, `false` to indicate failure (e.g. an invalid configuration state)
   * or a JSON object holding the configuration values to be stored on COYO
   * side.
   *
   * @param cb a callback that is executed every time a save request is received
   * @param validate a flag indicating if the JWT response should be validated
   * @returns a disconnect handler to stop listening to save events
   */
  onSave(cb: (respond: (data: boolean | JsonObject) => void, token: string) => void, validate = true): () => void {
    this.configAdapter.connect(cb, validate);
    return () => this.configAdapter.disconnect();
  }

  /**
   * Starts to observe the HTML element that matches the given selectors and
   * sends throttled update messages to COYO.
   *
   * @param config a flag indicating if the plugin configuration view should be observed
   * @param selectors the selectors to target the HTML element
   * @returns a disconnect handler to stop observing the HTML element
   */
  observeHeight(config = false, selectors = 'html'): () => void {
    const adapter = config ? this.heightConfigAdapter : this.heightAdapter;
    adapter.connect(selectors, 50);
    return () => adapter.disconnect();
  }
}
