# COYO Plug-in Adapter

## Develop & Build

To start working, run the `watch:build` task using [`npm`](https://docs.npmjs.com/getting-started/what-is-npm).

```sh
npm run watch:build
```

In another terminal tab/window, run the `watch:test` task:

```sh
npm run watch:test
```

These watch tasks make development much faster and more interactive. They will build and watch the entire project for
changes (to both the library source files and test source files). As you develop, you can add tests for new
functionality – which will initially fail – before developing the new functionality. Each time you save, any changes
will be rebuilt and retested. Since only changed files are rebuilt and retested, this workflow remains fast even for
large projects.

### Auto-fix and format project

To automatically fix `eslint` and `prettier` formatting issues, run:

```sh
npm run fix
```

### Generate your API docs

The src folder is analyzed and documentation is automatically generated using
[TypeDoc](https://github.com/TypeStrong/typedoc).

```sh
npm run doc
```

This command generates API documentation for your library in HTML format and opens it in a browser. Since types are
tracked by Typescript, there's no need to indicate types in JSDoc format. For more information, see the
[TypeDoc documentation](http://typedoc.org/guides/doccomments/).

## Test

To generate and view test coverage, run:

```sh
npm run cov
```

This will create an HTML report of test coverage – source-mapped back to Typescript – and open it in your default
browser.

## Deploy & Publish

To create a new version, run:

```sh
npm run release:{patch|minor|major}
```

This will build a new release via [standard-version](https://www.npmjs.com/package/standard-version) and automatically run the following steps:
* bumping version in *package.json*
* bumping version in *package-lock.json*
* outputting changes to *CHANGELOG.md*
* committing *package-lock.json* and *package.json* and *CHANGELOG.md*
* tagging release

Finally, push your changes via:

```sh
git push --follow-tags origin master
```

The CI will automatically publish the tagged version to *npm* and also redeploy the documentation at https://coyoapp.gitlab.io/plugins/adapter.
