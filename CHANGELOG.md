# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/coyoapp/plugins/adapter/compare/v1.1.0...v1.2.0) (2023-05-16)


### Features

* Certificates update ([c192991](https://gitlab.com/coyoapp/plugins/adapter/commit/c192991602aaea20d47763b8bbfe4a93f77bff89))

## [1.1.0](https://gitlab.com/coyoapp/plugins/adapter/compare/v1.0.0...v1.1.0) (2022-05-23)


### Features

* Certificates update ([ff53413](https://gitlab.com/coyoapp/plugins/adapter/commit/ff53413f8ee6b366c208df5d3c7d3710c5fb0c62))


### Bug Fixes

* updated all dependencies ([4885ed1](https://gitlab.com/coyoapp/plugins/adapter/commit/4885ed1cdf98abd657ee7265627c4aaa18a98805))

## [1.0.0](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.3.1...v1.0.0) (2022-03-17)


### Features

* whitelist additional certificate download url ([0c5d5ae](https://gitlab.com/coyoapp/plugins/adapter/commit/0c5d5aedd151fec5e46fceaca37eec8cce740929))

### [0.3.1](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.3.0...v0.3.1) (2021-08-25)


### Bug Fixes

* properly handle AggregateError in JWT verification ([164ea39](https://gitlab.com/coyoapp/plugins/adapter/commit/164ea39765254149b275c663912e215b9b0578b0))

## [0.3.0](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.2.3...v0.3.0) (2021-08-18)

### [0.2.3](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.2.2...v0.2.3) (2021-08-18)

### [0.2.2](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.2.1...v0.2.2) (2021-08-18)


### Features

* Support custom edit views via configUrls ([b5cc13a](https://gitlab.com/coyoapp/plugins/adapter/commit/b5cc13a0a7de43b16d6e04b315fd8f2204c00025))

### [0.2.1](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.2.0...v0.2.1) (2021-06-21)


### Bug Fixes

* **edit:** "Uncaught (in promise) undefined" ([1c2d0dc](https://gitlab.com/coyoapp/plugins/adapter/commit/1c2d0dcba63e2109773f9d1d824f145c15daab58))

## [0.2.0](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.1.7...v0.2.0) (2021-06-09)

### Features

* **COYOFOUR-15435:** add claims API ([93524eaa](https://gitlab.com/coyoapp/plugins/adapter/commit/93524eaa2ab6fa30839d9c6fa1aa3afa944efb0d))
* **COYOFOUR-15859:** add new certificates ([72b4e96d](https://gitlab.com/coyoapp/plugins/adapter/commit/72b4e96d802a462cdf0b803e33fbc52fa8d9cb4c))
* **COYOFOUR-15984:** add methods to access raw token ([f05c9f9b](https://gitlab.com/coyoapp/plugins/adapter/commit/f05c9f9b18c06c74cda52e522a01575eeac89fcc))
* updated dependencies ([bbd047a0](https://gitlab.com/coyoapp/plugins/adapter/commit/bbd047a004dfcd1e644023a836caf0d79f6313a1))

### [0.1.7](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.1.6...v0.1.7) (2021-03-18)


### Bug Fixes

* **COYOFOUR-15580:** fixed token.match is not a function in onEdit() (fixes [#1](https://gitlab.com/coyoapp/plugins/adapter/issues/1)) ([3ff2c76](https://gitlab.com/coyoapp/plugins/adapter/commit/3ff2c7640b1e5cdcd31b41aad934f1386146bd42))

### [0.1.6](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.1.5...v0.1.6) (2021-02-26)

### [0.1.5](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.1.4...v0.1.5) (2021-02-26)


### Bug Fixes

* **keys:** COYOFOUR-15399 - move to correct JWK keys for core and staging ([015e5c0](https://gitlab.com/coyoapp/plugins/adapter/commit/015e5c05b6d18965837e1f64120fb1e8ec6c393d))

### [0.1.4](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.1.3...v0.1.4) (2020-12-18)


### Bug Fixes

* **error:** remove duplicate error code from error messages ([02ae705](https://gitlab.com/coyoapp/plugins/adapter/commit/02ae70534384d4fc7be063da78b6bdabd5d9053e))

### [0.1.3](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.1.2...v0.1.3) (2020-12-18)


### Bug Fixes

* **keys:** fix compile errors ([8933305](https://gitlab.com/coyoapp/plugins/adapter/commit/89333052b346c266f9e8c21fb200f4a8842a1274))

### [0.1.2](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.1.1...v0.1.2) (2020-12-18)


### Bug Fixes

* **keys:** implement correct coyo keys ([ad66608](https://gitlab.com/coyoapp/plugins/adapter/commit/ad66608cd89699a1772cb5fe1c3594c42a4c87b9))

### [0.1.1](https://gitlab.com/coyoapp/plugins/adapter/compare/v0.1.0...v0.1.1) (2020-12-15)

## 0.1.0 (2020-11-25)
